// @flow
import * as React from "react";

import type { Component as ComponentType } from "../types";

type Props = {|
  component: ComponentType,
|};

// Question 1
// Récupérer le composant à afficher à partir des properties et la COMPONENT_MAP.

export default function Component({ component }: Props): React.Node {
  return <div className="preview-component">Missing component</div>;
}
